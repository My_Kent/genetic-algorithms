package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import KP.KPData;
import TSP.TSPData;

public class DiagramPanel extends JPanel {
	private double[] result;
	private int numOfPop;
	private double min;
	private double max;
	private int index;
	private double greedy;

	public void showDiagram(int index) {
		this.index = index;
		// TSP
		if (index == 0) {
			this.result = TSPData.result;
			// this.numOfPop = TSPData.numOfPop;

			// deal with min and max

			min = findMin(result);
			max = findMax(result);
			this.greedy = TSPData.greedyResult;
			if (max < greedy) {
				max = greedy;

			}
			if (min > greedy) {
				min = greedy;
			}
			repaint();
		}

		// KP
		if (index == 1) {
			this.result = KPData.result;
			min = findMin(result);
			max = findMax(result);
			this.greedy = KPData.greedyResult;
			if (greedy > max) {
				max = greedy;
			}

			if (greedy < min) {
				min = greedy;
			}

			repaint();
		}
	}

	private double findMax(double[] result) {
		double max = result[0];
		for (int i = 1; i < result.length; i++) {
			if (max < result[i]) {
				max = result[i];
			}
		}

		return max;

	}

	private double findMin(double[] result) {
		double min = result[0];
		for (int i = 1; i < result.length; i++) {
			if (min > result[i]) {
				min = result[i];
			}
		}

		return min;
	}

	protected void paintComponent(Graphics g) {
		if (result == null) {
			return;
		}
		// draw the diagram to show the tendency of the result

		super.paintComponent(g);

		g.setFont(new Font("TimesRoman", Font.PLAIN, 15));

		int width = getWidth();
		int height = getHeight();

		int xcoor1 = 100;
		int xcoor2 = width - 100;
		int xwidth = width - 200;

		int ycoor1 = height - 100;
		int ycoor2 = 100;
		int yheight = height - 240;

		g.setColor(Color.BLUE);
		// x-axis for generation
		g.drawLine(xcoor1, ycoor1, xcoor2, ycoor1);
		// y-axis for total distance that travelled
		g.drawLine(xcoor1, ycoor1, xcoor1, ycoor2);

		g.setColor(Color.BLACK);

		int size = result.length;

		int temp = size / 10;

		for (int i = 0; i < size - 1; i++) {
			int xinterval1 = i * xwidth / size;
			int x1 = xinterval1 + xcoor1;
			int yinterval1 = (int) ((result[i] - min) * yheight / (max - min));
			int y1 = ycoor1 - yinterval1;

			int xinterval2 = (i + 1) * xwidth / size;
			int x2 = xinterval2 + xcoor1;
			int yinterval2 = (int) ((result[i + 1] - min) * yheight / (max - min));
			int y2 = ycoor1 - yinterval2;

			g.drawLine(x1, y1, x2, y2);
			if (i % temp == 0) {
				g.drawString("(" + i + "," + result[i] + ")", x1 - 5, y1 - 20);
			}

		}

//		if (index == 1) {
			g.setColor(Color.RED);
			int yinterval = (int) ((greedy - min) * yheight / (max - min));
			int y1 = ycoor1 - yinterval;
			g.drawLine(xcoor1, y1, xcoor1 + xwidth, y1);
			double temp1 = ((int) greedy * 100) / 100.0;
			g.drawString("Greedy Search Result: " + temp1, xcoor1 + xwidth
					- 200, y1 - 30);
			g.setColor(Color.BLACK);
//		}

		int y = ycoor1
				- (int) ((result[size - 1] - min) * yheight / (max - min));
		// g.drawString("("+ (size-1)+","+result[size-1]+")", xwidth, y);
		g.drawString("Generation", xcoor2 - 20, ycoor1 + 30);
		if (index == 0) {
			
			
			
			g.drawString("Total Distance that travelled", xcoor1 - 20,
					ycoor2 - 20);

		}
		if (index == 1) {
			g.drawString("Total benifit ", xcoor1 - 20, ycoor2 - 20);
			g.drawString("The selected items are: " + KPData.result1, 50,
					height - 50);
		}

	}

}
