package GUI;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

public class GuideFrame extends JFrame {
	private MainFrame main;

	public GuideFrame() {
		setLayout(null);

		// label for the title of this page
		JLabel jlblTitle = new JLabel("Guidance");
		Font font1 = new Font("SansSerif", Font.BOLD + Font.ITALIC, 18);
		Font font2 = new Font("SansSerif", Font.BOLD, 13);
		jlblTitle.setFont(font1);
		jlblTitle.setBorder(new TitledBorder(""));
		jlblTitle.setBounds(10, 10, 1180, 80);
		add(jlblTitle);

		JPanel type = new JPanel();
		type.setBorder(new TitledBorder("Type"));
		type.setLayout(new GridLayout(1, 1, 5, 5));
		String context = "For both TSP and KP, the size of population and number of generation can be specified by entering the number into the related text field on the relative windows, the type of the number should be postive integers.\n"
				+ "The mutation rate can be specified by entering the number into the related text field on the relative windows, the type of mutation rate should be a postive float number with range 0 to 1.\n"
				+ "For Knapsack Problem, the maximum capacity of the knapsack can be by entering the number into the related text field on the relative windows, the type of the capacity should be a postive float number.\n"
				+ "Any inconsistent number entering would rise a warn window to remind the type constraint on the parameters.";
		JTextArea text1 = new JTextArea(context);
		text1.setLineWrap(true);
		type.add(text1);

		type.setBounds(10, 100, 1180, 200);
		add(type);

		JPanel file = new JPanel();
		file.setBorder(new TitledBorder("File Format"));
		file.setLayout(new GridLayout(1, 1, 5, 5));
		String context2 = "The input format for TSP:\n"
				+ "Data is display by rows and columns, for example, if there are totally five cities need to be travel, then the input data should be like\n"
				+ "0 1 2 3 4 \n"
				+ "1 0 5 2 3 \n"
				+ "2 5 0 3 1 \n"
				+ "3 2 3 0 5 \n"
				+ "4 3 1 5 0 \n"
				+ "Where the first row represents the distance between all the cities to the first city, each number is separated by white space; and the second row represents distance between the second city and all cities, and so the third, fourth and fifth rows.\n"
				+ "\nThe input format for KP:\n"
				+ "Each item is specified with item name, benefit and volume sepated by white space;and items are sepated by line breaker. For example, three items are considered to be added into the knapsack then the data in the txt should be like\n"
				+ "apple 100 50\n"
				+ "banana 50 100\n"
				+ "orange 80 80\n"
				+ "shows that the first item named apple with 100 benefit and 50 volume, the second item called banana with 50 benefit and 100 volume and the third item named orange with 80 benefit and 80 volume.";
		JTextArea text2 = new JTextArea(context2);
		text2.setLineWrap(true);
		file.add(text2);
		file.setBounds(10, 310, 1180, 400);
		add(file);

		JButton previous = new JButton("Previous");
		previous.setBounds(200, 730, 100, 30);
		add(previous);
		previous.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main = new MainFrame();
				main.setLocation(100, 100);
				main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				main.setTitle("Main");
				main.setSize(1200, 900);
				main.setVisible(true);
				dispose();
			}
		});

	}
}
