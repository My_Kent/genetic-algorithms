package GUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MainFrame extends JFrame {
	// two frames that used in the next step
	private TSPFrame tsp = new TSPFrame();
	private KPFrame kp = new KPFrame();
	private GuideFrame guide = new GuideFrame();

	// create button for uses to choose the program to solve
	

	public MainFrame() {
		setLayout(null);
		
		
		
		ImageIcon ga = new ImageIcon("ga.jpg");
		JLabel img = new JLabel(ga);
		img.setBounds(50, 50, 1100, 550);
		
		add(img);
		
		JButton jbtGuide = new JButton("Guidance");
		jbtGuide.setBounds(110, 600, 100, 30);
		JButton jbtTSP = new JButton("Travelling Salesman Problem");
		jbtTSP.setBounds(330, 600, 350, 30);
		JButton jbtKP = new JButton("Knapsack Problem");
		jbtKP.setBounds(700, 600, 350, 30);
		
		add(jbtTSP);
		add(jbtKP);
		add(jbtGuide);
		
		
		
		jbtTSP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tsp.setLocation(100, 100);
				tsp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				tsp.setTitle("Travalling Salesman Problem");
				tsp.setSize(1200, 900);
				tsp.setVisible(true);
				// setVisible(false);
				dispose();
			}
		});

		jbtKP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kp.setLocation(100, 100);
				kp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				kp.setTitle("Knapsack Problem");
				kp.setSize(1200, 900);
				kp.setVisible(true);
				dispose();
			}
		});
		jbtGuide.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				guide.setLocation(100, 100);
				guide.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				guide.setTitle("Guidance");
				guide.setSize(1200, 900);
				guide.setVisible(true);
				dispose();
			}
		});
	}

	public static void main(String[] args) {
		MainFrame frame = new MainFrame();
		frame.setLocation(100, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Main menu");
		frame.setSize(1200, 900);
		frame.setVisible(true);
	}

}
