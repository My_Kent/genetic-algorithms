package GUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import TSP.City;
import TSP.CityList;
import TSP.TSPData;
import Utility.ReadFile;
import Utility.TSPHandler;

public class TSPFrame extends JFrame {
	private MainFrame main;
	private TSPResult result;

	// content from the frame
	private boolean fromfile = false;
	private String[] cities;
	private int sizeOfPop = 0;
	private int numOfGene = 0;
	private int selectMethod = 0;
	private int crossoverMethod = 0;
	private boolean cross = false;
	private double crossoverRate = -1;
	private double mutateRate = -1;
	private boolean isElitism = true;

	public TSPFrame() {

		setLayout(null);

		// Label for the title of this page
		JLabel jlblTitle = new JLabel("Travelling Salesman Problem");
		Font font1 = new Font("SansSerif", Font.BOLD + Font.ITALIC, 18);
		Font font2 = new Font("SansSerif", Font.BOLD, 13);
		jlblTitle.setFont(font1);
		jlblTitle.setBorder(new TitledBorder(""));
		jlblTitle.setBounds(10, 10, 1180, 80);
		add(jlblTitle);

		// Step 1
		JLabel jlblStep1 = new JLabel(
				"1. Add the cities that you are going to travel: ");
		jlblStep1.setFont(font2);
		jlblStep1.setBounds(20, 120, 400, 30);
		add(jlblStep1);
		final JFileChooser file = new JFileChooser();
		JButton addBtn = new JButton("Choose Input File");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				file.showDialog(null, null);
			}
		});
		addBtn.setBounds(500, 122, 180, 30);
		add(addBtn);

		// Step 2
		JLabel jlblStep2 = new JLabel(
				"2. Determine the population size and the total number of generation the program is going to generate: ");
		jlblStep2.setFont(font2);
		jlblStep2.setBounds(20, 180, 700, 30);
		add(jlblStep2);

		JLabel jlbl21 = new JLabel("Size of population: ");
		jlbl21.setFont(font2);
		jlbl21.setBounds(50, 220, 150, 30);
		add(jlbl21);

		final JTextField jtxt21 = new JTextField(5);
		jtxt21.setBounds(200, 220, 100, 30);
		add(jtxt21);

		JLabel jlbl22 = new JLabel("Total number of generation: ");
		jlbl22.setFont(font2);
		jlbl22.setBounds(500, 220, 200, 30);
		add(jlbl22);
		final JTextField jtxt22 = new JTextField(5);
		jtxt22.setBounds(700, 220, 100, 30);
		add(jtxt22);

		// Step 3

		JLabel jlblStep3 = new JLabel("3. Choose the selection method: ");
		jlblStep3.setFont(font2);
		jlblStep3.setBounds(20, 260, 240, 30);
		add(jlblStep3);
		final JComboBox jcb31 = new JComboBox(new Object[] {
				"Roulette Wheel Section", "Rank Selection",
				"Tournament Selection", "Group Selection" });
		jcb31.setBounds(500, 262, 240, 30);
		add(jcb31);

		// Step 4
		JLabel jlblStep4 = new JLabel("4. Choose the crossover method: ");
		jlblStep4.setFont(font2);
		jlblStep4.setBounds(20, 320, 240, 30);
		add(jlblStep4);
		final JComboBox jcb41 = new JComboBox(new Object[] {
				"Single point crossover", "Two point crossover" });
		jcb41.setBounds(500, 322, 240, 30);
		add(jcb41);

		// Step 5: Define the mutation rate

		JLabel jlblStep5 = new JLabel("5. Specify the mutation rate: ");
		jlblStep5.setFont(font2);
		jlblStep5.setBounds(20, 380, 240, 30);
		add(jlblStep5);
		final JTextField jtxt5 = new JTextField("0.05");

		jtxt5.setBounds(500, 380, 150, 30);
		add(jtxt5);

		// Step 6: Define whether elitism method is used

		JLabel jlblStep6 = new JLabel("6. Whether elitism: ");
		jlblStep6.setFont(font2);
		jlblStep6.setBounds(20, 440, 240, 30);
		add(jlblStep6);

		JRadioButton iselitism = new JRadioButton("Yes");
		iselitism.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isElitism = true;
			}
		});

		JRadioButton notelitism = new JRadioButton("No");
		notelitism.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isElitism = false;
			}
		});
		ButtonGroup group3 = new ButtonGroup();
		group3.add(iselitism);
		group3.add(notelitism);

		iselitism.setBounds(300, 442, 90, 30);
		add(iselitism);
		notelitism.setBounds(500, 442, 90, 30);
		add(notelitism);

		// button
		JButton prevxBtn = new JButton("Previous");
		prevxBtn.setBounds(300, 600, 100, 30);
		add(prevxBtn);
		JButton runBtn = new JButton("Run");
		runBtn.setBounds(600, 600, 100, 30);
		add(runBtn);

		// button link to the main frame
		prevxBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main = new MainFrame();
				main.setVisible(true);
				main.setLocation(100, 100);
				main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				main.setTitle("Main menu");
				main.setSize(1200, 900);
				main.setVisible(true);
				dispose();
			}
		});

		// start to run the algorithm
		runBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// get the arguments for TSP
				try {
					File selectedFile = file.getSelectedFile();
					String file_path = selectedFile.getAbsolutePath();

					ReadFile file = new ReadFile(file_path);
					cities = file.getContent();

					City[] cityList = getCities(cities);
					sizeOfPop = Integer.parseInt(jtxt21.getText());
					numOfGene = Integer.parseInt(jtxt22.getText());
					selectMethod = jcb31.getSelectedIndex();
					crossoverMethod = jcb41.getSelectedIndex();

					crossoverRate = Math.random();
					while (crossoverRate <= 0.3 || crossoverRate >= 0.7) {
						crossoverRate = Math.random();
					}

					// TODO to check that the mutateRate r should 0 < r < 1
					mutateRate = Double.parseDouble(jtxt5.getText());

					TSPData.cityList = cityList;
					TSPData.numOfPop = sizeOfPop;
					TSPData.generation = numOfGene;
					TSPData.selectMtd = selectMethod;
					TSPData.crossMtd = crossoverMethod;
					TSPData.mutationRate = mutateRate;
					TSPData.isElism = isElitism;

					TSPHandler tsp = new TSPHandler(cityList, sizeOfPop,
							numOfGene, selectMethod, crossoverMethod,
							crossoverRate, mutateRate, isElitism);

					result = new TSPResult();
					result.setVisible(true);
					result.setLocation(100, 100);
					result.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					result.setTitle("TSP Result");
					result.setSize(1200, 900);
					result.setVisible(true);
					dispose();
				} catch (Exception ex) {
					JOptionPane
							.showMessageDialog(
									null,
									"Wrong data format\nThe type of size of population should be postive Integer,\nThe type of number of generation should be positive Integer,\nAnd the mutation rate should be a double number ranged in 0 to 1");

				}

			}

			private City[] getCities(String[] cities) {

				// String delims = "[ \t]+";
				// String[] CityName = cities[0].split(delims);
				// CityList.distance = new double[CityName.length -
				// 1][CityName.length - 1];
				// // store the information about distance between two cities in
				// // the two dimensional array
				// for (int i = 1; i < cities.length; i++) {
				// String[] distance = cities[i].split(delims);
				// for (int j = 1; j < distance.length; j++) {
				// double temp = Double.parseDouble(distance[j]);
				// CityList.distance[i - 1][j - 1] = temp;
				// }
				// }
				// City[] cityList = new City[cities.length - 1];
				// for (int i = 0; i < CityName.length - 1; i++) {
				// cityList[i] = new City(i, CityName[i + 1]);
				// }
				// return cityList;

				String delims = "[ \t]+";
				CityList.distance = new double[cities.length][cities.length];
				for (int i = 0; i < cities.length; i++) {
					String[] distance = cities[i].split(delims);
					for (int j = 0; j < cities.length; j++) {
						double temp = Double.parseDouble(distance[j]);
						CityList.distance[i][j] = temp;
					}
				}
				City[] cityList = new City[cities.length];
				for (int i = 0; i < cities.length; i++) {
					cityList[i] = new City(i, "c" + i);
				}
				return cityList;

			}

		});
	}

}
