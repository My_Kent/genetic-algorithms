package GUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import KP.Item;
import KP.KPData;
import TSP.TSPData;
import Utility.KPHandler;
import Utility.ReadFile;

public class KPFrame extends JFrame {
	private MainFrame main;

	// content from the frame
	private boolean fromfile = false;
	private String[] items;
	private int sizeOfPop = 0;
	private int numOfGene = 0;
	private int selectMethod = 0;
	private int crossoverMethod = 0;
	private boolean cross = false;
	private double crossoverRate = -1;
	private double mutateRate = -1;
	private boolean isElitism = true;
	private double totalVolume = 0;

	public KPFrame() {

		setLayout(null);

		// Label for the title of this page
		JLabel jlblTitle = new JLabel("Knapsack Problem");
		Font font1 = new Font("SansSerif", Font.BOLD + Font.ITALIC, 18);
		Font font2 = new Font("SansSerif", Font.BOLD, 13);
		jlblTitle.setFont(font1);
		jlblTitle.setBorder(new TitledBorder(""));
		jlblTitle.setBounds(10, 10, 1180, 80);
		add(jlblTitle);

		// Step 1
		JLabel jlblStep1 = new JLabel(
				"1. Add the items that is considered to put in the bag ");
		jlblStep1.setFont(font2);
		jlblStep1.setBounds(20, 120, 400, 30);
		add(jlblStep1);
		final JFileChooser file = new JFileChooser();
		JButton addBtn = new JButton("Choose Input File");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				file.showDialog(null, null);
			}
		});
		addBtn.setBounds(500, 122, 180, 30);
		add(addBtn);

		// Step 2: define the population size and the number of generation

		JLabel jlblStep2 = new JLabel(
				"2. Determine the population size and the total number of generation the program is going to generate: ");
		jlblStep2.setFont(font2);
		jlblStep2.setBounds(20, 180, 700, 30);
		add(jlblStep2);

		JLabel jlbl21 = new JLabel("Size of population: ");
		jlbl21.setFont(font2);
		jlbl21.setBounds(50, 220, 150, 30);
		add(jlbl21);

		final JTextField jtxt21 = new JTextField(5);
		jtxt21.setBounds(200, 220, 100, 30);
		add(jtxt21);

		JLabel jlbl22 = new JLabel("Total number of generation: ");
		jlbl22.setFont(font2);
		jlbl22.setBounds(500, 220, 200, 30);
		add(jlbl22);
		final JTextField jtxt22 = new JTextField(5);
		jtxt22.setBounds(700, 220, 100, 30);
		add(jtxt22);

		// Step 3: choose the selection method

		JLabel jlblStep3 = new JLabel("3. Choose the selection method: ");
		jlblStep3.setFont(font2);
		jlblStep3.setBounds(20, 260, 240, 30);
		add(jlblStep3);
		final JComboBox jcb31 = new JComboBox(new Object[] {
				"Routtle Wheel Section", "Rank Selection",
				"Tournament Selection", "Group Selection" });
		jcb31.setBounds(500, 262, 240, 30);
		add(jcb31);

		// Step 4: choose the crossover method

		JLabel jlblStep4 = new JLabel("4. Choose the crossover method: ");
		jlblStep4.setFont(font2);
		jlblStep4.setBounds(20, 320, 240, 30);
		add(jlblStep4);
		final JComboBox jcb41 = new JComboBox(new Object[] {
				"Single point crossover", "Two point crossover" });
		jcb41.setBounds(500, 322, 240, 30);
		add(jcb41);

		// Step 5: Define the mutation rate

		JLabel jlblStep5 = new JLabel("5. Specify the mutation rate: ");
		jlblStep5.setFont(font2);
		jlblStep5.setBounds(20, 380, 240, 30);
		add(jlblStep5);
		final JTextField jtxt5 = new JTextField("0.001");

		jtxt5.setBounds(500, 380, 150, 30);
		add(jtxt5);

		// Step 6: Define whether elitism method is used

		JLabel jlblStep6 = new JLabel("6. Whether elitism: ");
		jlblStep6.setFont(font2);
		jlblStep6.setBounds(20, 440, 240, 30);
		add(jlblStep6);

		JRadioButton iselitism = new JRadioButton("Yes");
		iselitism.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isElitism = true;
			}
		});

		JRadioButton notelitism = new JRadioButton("No");
		notelitism.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isElitism = false;
			}
		});
		ButtonGroup group3 = new ButtonGroup();
		group3.add(iselitism);
		group3.add(notelitism);

		iselitism.setBounds(300, 442, 90, 30);
		add(iselitism);
		notelitism.setBounds(500, 442, 90, 30);
		add(notelitism);

		// Step 7: Define the maximum capacity of the bag

		JLabel jlblStep7 = new JLabel("7. Specify the total volume of the bag");
		jlblStep7.setFont(font2);
		jlblStep7.setBounds(20, 500, 280, 30);
		add(jlblStep7);
		final JTextField jtxt71 = new JTextField(5);
		jtxt71.setBounds(500, 500, 150, 30);
		add(jtxt71);

		// button
		JButton prevxBtn = new JButton("Previous");
		prevxBtn.setBounds(300, 600, 100, 30);
		add(prevxBtn);

		JButton runBtn = new JButton("Run");
		runBtn.setBounds(600, 600, 100, 30);
		add(runBtn);

		prevxBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main = new MainFrame();
				main.setVisible(true);
				main.setLocation(100, 100);
				main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				main.setTitle("Main menu");
				main.setSize(1200, 900);
				main.setVisible(true);
				dispose();
				// setVisible(false);
			}
		});

		// start to run the algorithm
		runBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					File selectedFile = file.getSelectedFile();
					String file_path = selectedFile.getAbsolutePath();

					ReadFile file = new ReadFile(file_path);
					items = file.getContent();

					Item[] itemList = getItems(items);
					sizeOfPop = Integer.parseInt(jtxt21.getText());
					if(sizeOfPop<=0){
						throw new Exception();
					}
					
					numOfGene = Integer.parseInt(jtxt22.getText());
					if(numOfGene <= 0){
						throw new Exception();
					}
					
					selectMethod = jcb31.getSelectedIndex();
					crossoverMethod = jcb41.getSelectedIndex();
					
					while(crossoverRate <=0.3 || crossoverRate>=0.7){
						crossoverRate = Math.random();
					}

					mutateRate = Double.parseDouble(jtxt5.getText());
					if(mutateRate <= 0 || mutateRate>=1){
						throw new Exception();
					}
					
					totalVolume = Double.parseDouble(jtxt71.getText());
					if(totalVolume < 0){
						throw new Exception();
					}
					KPData.itemlist = itemList;
					KPData.numOfPop = sizeOfPop;
					KPData.generation = numOfGene;
					KPData.selectMtd = selectMethod;
					KPData.crossMtd = crossoverMethod;
					KPData.mutationRate = mutateRate;
					KPData.isElism = isElitism;
					KPData.totalVolume = totalVolume;

					KPHandler kp = new KPHandler(itemList, sizeOfPop,
							numOfGene, selectMethod, crossoverMethod,
							crossoverRate, mutateRate, isElitism, totalVolume);

					KPResult result = new KPResult();
					result.setVisible(true);
					result.setLocation(100, 100);
					result.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					result.setTitle("KP Result");
					result.setSize(1200, 900);
					result.setVisible(true);
					dispose();
				} catch (Exception ex) {
					JOptionPane
							.showMessageDialog(
									null,
									"Wrong data format\nThe type of size of population should be postive Integer,\nThe type of number of generation should be positive Integer,\nThe mutation rate should be a double number ranged in 0 to 1,\nAnd the capacity should be a positive double number");
				}

			}

			private Item[] getItems(String[] items) {
				int size = items.length;
				Item[] itemList = new Item[size];
				String delims = "[ ]+";
				for (int i = 0; i < size; i++) {
					String[] arg = items[i].split(delims);
					String itemName = arg[0];
					double benefit = Double.parseDouble(arg[1]);
					double volume = Double.parseDouble(arg[2]);
					itemList[i] = new Item(itemName, benefit, volume);
				}
				return itemList;
			}
		});

	}
}
