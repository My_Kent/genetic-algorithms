package GUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import TSP.TSPData;
import Utility.KPHandler;
import Utility.TSPHandler;

public class TSPResult extends JFrame {

	public TSPResult() {

		int sizeOfPop = TSPData.numOfPop;
		int numOfGene = TSPData.generation;
		int selectMtd1 = TSPData.selectMtd;
		int crossMtd1 = TSPData.crossMtd;
		double mutationRate = ((int) (TSPData.mutationRate * 1000)) / 1000.0;
		String selectMtd2 = "";
		String crossMtd2 = "";

		boolean isElitism = TSPData.isElism;
		switch (selectMtd1) {
		case 0:
			selectMtd2 = "Routtle Wheel Section";
			break;
		case 1:
			selectMtd2 = "Rank Selection";
			break;
		case 2:
			selectMtd2 = "Tournament Selection";
			break;
		case 3:
			selectMtd2 = "Group Selection";
			break;
		default:
			break;
		}

		switch (crossMtd1) {
		case 0:
			crossMtd2 = "Single point crossover";
			break;
		case 1:
			crossMtd2 = "Two point crossover";
			break;
		default:
			break;
		}

		String isElitism1 = "";
		if (isElitism) {
			isElitism1 = "YES";
		} else {
			isElitism1 = "NO";
		}

		JPanel parameter = new JPanel();
		parameter.setBorder(new TitledBorder("Parameter of TSP"));
		parameter.setLayout(new GridLayout(4, 1, 5, 5));

		JLabel txt11 = new JLabel("1.The size of population is: " + sizeOfPop
				+ "            " + "2. Total number of generation is: "
				+ numOfGene);
		JLabel txt12 = new JLabel("3.Selection method chosen: " + selectMtd2
				+ "            " + "4.Crossover method chosen: " + crossMtd2);
		JLabel txt13 = new JLabel("5.Mutation Rate: " + mutationRate
				+ "            " + "6.Whether elitism: " + isElitism1);

		parameter.add(txt11);
		parameter.add(txt12);
		parameter.add(txt13);

		add(parameter, BorderLayout.NORTH);

		final DiagramPanel p2 = new DiagramPanel();
		p2.showDiagram(0);
		p2.setBorder(new TitledBorder("Result"));
		add(p2, BorderLayout.CENTER);

		JPanel p3 = new JPanel();
		p3.setBorder(new TitledBorder(""));
		JButton preBtn = new JButton("Previous");
		JButton runBtn = new JButton("Run Again");
		p3.add(preBtn);
		p3.add(runBtn);
		add(p3, BorderLayout.SOUTH);

		preBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TSPFrame frame = new TSPFrame();
				frame.setVisible(true);
				frame.setLocation(100, 100);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setTitle("Travalling Salesman Problem");
				frame.setSize(1200, 900);
				frame.setVisible(true);
				dispose();
			}
		});

		runBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double crossoverRate = Math.random();
				while (crossoverRate <= 0.1 || crossoverRate >= 0.9) {
					crossoverRate = Math.random();
				}
				TSPHandler tsp = new TSPHandler(TSPData.cityList,
						TSPData.numOfPop, TSPData.generation,
						TSPData.selectMtd, TSPData.crossMtd, crossoverRate,
						TSPData.mutationRate, TSPData.isElism);
				p2.showDiagram(0);
			}
		});

	}

}
