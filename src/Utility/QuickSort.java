package Utility;


public class QuickSort {
	// sort individual set ascending
	public static void quickSort(Individual[] pop){
		quickSort(pop, 0, pop.length-1);
	}
	
	private static void quickSort(Individual[] pop, int first, int last){
		if(last > first){
			int pivotIndex = partition(pop, first, last);
			quickSort(pop, first, pivotIndex - 1);
			quickSort(pop, pivotIndex + 1, last);
		}
	}
	
	private static int partition(Individual[] pop, int first, int last){
		Individual pivot = pop[first];
		double fitness  = pivot.getFitness();
		int low = first + 1;
		int high = last;
		while(high > low){
			// search forward from left
			while(low <= high && pop[low].getFitness() <= fitness)
				low++;
			
			while(low <= high && pop[high].getFitness() > fitness)
				high--;
			
			if(high > low){
				Individual temp = pop[high];
				pop[high] = pop[low];
				pop[low] = temp;
			}
		}
		
		while(high > first && pop[high].getFitness() >= fitness)
			high--;
		
		if(fitness > pop[high].getFitness()){
			pop[first] = pop[high];
			pop[high] = pivot;
			return high;
		}
		else{
			return first;
		}
		
	}
	
}
