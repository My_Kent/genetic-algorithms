package Utility;

import KP.KCrossover;
import KP.KPData;
import KP.KPGreedy;
import KP.Package;
import KP.Item;
import KP.ItemManager;
import KP.KPopulation;
import KP.KSelection;

public class KPHandler {
	private Item[] items;
	private int size = 0;
	private int number = 0;
	private int selectMtd = 0;
	private int crossMtd = 0;
	private double crossRate = 0;
	private double mutateRate = 0;
	private boolean isElitism = true;
	private double totalVolume = 0;

	public KPHandler(Item[] items, int size, int number, int selectMtd,
			int crossMtd, double crossRate, double mutateRate,
			boolean isElitism, double totalVolume) {
		this.items = items;
		this.size = size;
		this.number = number;
		this.selectMtd = selectMtd;
		this.crossMtd = crossMtd;
		this.crossRate = crossRate;
		this.mutateRate = mutateRate;
		this.isElitism = isElitism;
		this.totalVolume = totalVolume;

		addItems(items);

		// test 1000 times see the performance differences between GA and greedy
		// int time = 1000; 
		// int count = 0;
		// for (int j = 0; j <= time; j++) { 
			
			
			
		//	System.out.println(j);

			KPGreedy greedy = new KPGreedy();

			System.out.println("Result of greedy search is "
					+ KPData.greedyResult);

			KPopulation pop = new KPopulation(size, totalVolume, true);
			System.out.println("This is the first generation: ");
			System.out.println(pop.getFittest().toString());
			System.out
					.println("-------------------------------------------------------");

			KPData.result = new double[number];
			KPData.result[0] = pop.getFittest().getFitness();

			for (int i = 0; i < number; i++) {
				pop = nextGeneration(pop);
				KPData.result[i] = pop.getFittest().getFitness();

			}

			System.out.println("This is the last generation: ");
			System.out.println(pop.getFittest().toString());
//			System.out
//					.println("-------------------------------------------------------");

			KPData.result1 = pop.getFittest().toString();
			
			// 
		//	if(pop.getFittest().getFitness() > KPData.greedyResult){
		//		count++;
		//	}
			
		//}
		//System.out.println("generation: "  + number +  " with "+ count + " number better performance");

	}

	private KPopulation nextGeneration(KPopulation pop) {
		KPopulation offspring = new KPopulation(pop.getSize(),
				pop.getMaxVolume(), false);

		if (isElitism) {

			int elism = (int) (pop.getSize() * 0.1);

			int length = pop.getSize();
			Package[] tr = new Package[length];
			// sort
			for (int i = 0; i < length; i++) {
				tr[i] = pop.getIndividual(i);
			}
			QuickSort.quickSort(tr);
			for (int i = 0; i < elism; i++) {
				offspring.addIndividual(tr[length - 1 - i]);
			}

			// Package temp = pop.getFittest();
			// offspring.addIndividual(temp);

			for (int i = 1; i < pop.getSize(); i++) {
				Package parent1 = KSelection.select(pop, selectMtd);
				Package parent2 = KSelection.select(pop, selectMtd);

				Package child = KCrossover.crossover(crossMtd, crossRate,
						parent1, parent2);
				child = mutate(child, mutateRate);
				child.makeValidIndividual(totalVolume);
				offspring.addIndividual(child);
			}

		} else {
			for (int i = 0; i < pop.getSize(); i++) {
				Package parent1 = KSelection.select(pop, selectMtd);
				Package parent2 = KSelection.select(pop, selectMtd);

				Package child = KCrossover.crossover(crossMtd, crossRate,
						parent1, parent2);
				child = mutate(child, mutateRate);
				child.makeValidIndividual(totalVolume);
				offspring.addIndividual(child);
			}
		}

		return offspring;
	}

	private Package mutate(Package child, double mutateRate) {
		for (int i = 0; i < ItemManager.getSize(); i++) {
			double isMutated = Math.random();
			if (isMutated < mutateRate) {
				int value = child.getValueN(i);
				if (value == 1) {
					child.setValueN(i, 0);
				} else {
					child.setValueN(i, 1);
				}
			}
		}
		return child;
	}

	private void addItems(Item[] items) {
		ItemManager.clear();
		int size = items.length;
		for (int i = 0; i < size; i++) {
			ItemManager.addItem(items[i]);
		}
	}

}
