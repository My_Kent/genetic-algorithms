package Utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {
	private String path;
	private String[] content;
	public String[] getContent() {
		return content;
	}

	private int line = 0;

	public ReadFile(String file_path) {
		this.path = file_path;
		try {
			openFile();
			content = new String[line];
			readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void readLine() throws IOException {
		FileReader fr = new FileReader(path);
		BufferedReader textReader = new BufferedReader(fr);
		for (int i = 0; i < line; i++) {
			content[i] = textReader.readLine();
		}
	}

	public void openFile() throws IOException {
		FileReader fr = new FileReader(path);
		BufferedReader textReader = new BufferedReader(fr);
		String aLine;
		int numberOfLines = 0;
		while ((aLine = textReader.readLine()) != null) {
			numberOfLines++;
		}
		textReader.close();
		line = numberOfLines;
	}

	// public static void main(String[] args){
	// ReadFile rd = new
	// ReadFile("/Users/apple/Documents/JavaWorkSpace/cities.txt");
	// }
}
