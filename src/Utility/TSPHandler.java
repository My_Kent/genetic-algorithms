package Utility;

import TSP.City;
import TSP.CityList;
import TSP.TCrossover;
import TSP.TPopulation;
import TSP.TSPData;
import TSP.TSPGreedy;
import TSP.TSelection;
import TSP.Travel;

public class TSPHandler {
	private City[] cities;
	private int size = 0;
	private int number = 0;
	private int selectMtd = 0;
	private int crossMtd = 0;
	private double crossRate = 0;
	private double mutateRate = 0;
	private boolean isElitism = true;

	public TSPHandler() {

	}

	public TSPHandler(City[] cities, int sizeOfPop, int numOfGene,
			int selectMethod, int crossMethod, double crossRate,
			double mutateRate, boolean isElitism) {
		this.cities = cities;
		this.size = sizeOfPop;
		this.number = numOfGene;
		this.selectMtd = selectMethod;
		this.crossMtd = crossMethod;
		this.crossRate = crossRate;
		this.mutateRate = mutateRate;
		this.isElitism = isElitism;

		// add the cities that users are going to travel
		addCities(cities);

		
		// running greedy search algorithm
		TSPGreedy tsp = new TSPGreedy();
		// double 	distance = TSPData.greedyResult;		
		
		int count = 1000;  //delete
		int count1 = 0;
		double startTime = System.currentTimeMillis();
		
		// double[][] data = new double[count][number];	
		
		
		// for (int j = 0; j < count; j++) {	
		//	System.out.println(" " + j);
			TPopulation pop = new TPopulation(size, true);
			TSPData.result = new double[number];
			Travel fittest = pop.getFittest();

			System.out.println("First generation:\n" + fittest.toString());
			TSPData.result[0] = fittest.getDistance();
			
			for (int i = 0; i < number; i++) {
			//	data[j][i] = pop.getFittest().getDistance();	
				pop = nextGeneration(pop);
				TSPData.result[i] = pop.getFittest().getDistance();
				
			}
			
			// double distance1 = pop.getFittest().getDistance();
			// if(distance1 < distance){count1++;}
			
			Travel fittest1 = pop.getFittest();
			System.out.println("Last generation:\n" + fittest1.toString());
			System.out.println("Greedy result: " + TSPData.greedyResult);
			
			
			
			
		//}	
		
		
		/*		for testing purpose
		
		double endTime = System.currentTimeMillis();
		double runTime = (endTime - startTime)/1000;
		
		
		System.out.println("With " + count1 + " result better than greedy");
		
		System.out.println("Result of the selection with index: " + selectMtd);
		System.out.println("With totally runTime: " + runTime);
		System.out.println("-----------------------------------");
		
		double[] data1 = new double[number];	//delete
		for(int i = 0; i < number;i++){			//delete
			double sum = 0;						//delete
			for(int j = 0; j < count; j++){		//delete
				sum += data[j][i];				//delete
			}									//delete
			data1[i] = sum/count;				//delete
			System.out.println(data1[i]);		//delete
		}										
				
		System.out.println("-----------------------------------");
		System.out.println("End");
		
		*/
	}

	private void addCities(City[] cities) {
		CityList.clear();
		int size = cities.length;
		for (int i = 0; i < size; i++) {
			CityList.addCity(cities[i]);
		}

	}

	private TPopulation nextGeneration(TPopulation pop) {
		TPopulation offspring = new TPopulation(pop.getSize(), false);
		if (isElitism) {

			int length = pop.getSize();
			int elism = (int) (length * 0.1);
			Travel[] tr = new Travel[length];
			for (int i = 0; i < length; i++) {
				tr[i] = pop.getTravel(i);
			}
			QuickSort.quickSort(tr);

			for (int i = 0; i < elism; i++) {
				offspring.addTravel(i, tr[length - 1 - i]);
			}

			Travel fittest = pop.getFittest();
			offspring.addTravel(0, fittest);

			for (int i = elism; i < offspring.getSize(); i++) {
				Travel parent1 = TSelection.select(pop, selectMtd);
				// Travel parent1 = pop.getTravel(i);
				Travel parent2 = TSelection.select(pop, selectMtd);
				Travel child = TCrossover.crossover(crossMtd, crossRate,
						parent1, parent2);
				child = mutate(mutateRate, child);
				offspring.addTravel(i, child);
			}

		} else {

			for (int i = 0; i < offspring.getSize(); i++) {
				Travel parent1 = TSelection.select(pop, selectMtd);
				Travel parent2 = TSelection.select(pop, selectMtd);
				Travel child = TCrossover.crossover(crossMtd, crossRate,
						parent1, parent2);
				child = mutate(mutateRate, child);
				offspring.addTravel(i, child);
			}
		}
		return offspring;
	}

	public Travel mutate(double mutateRate2, Travel child) {
		for (int i = 0; i < CityList.numberOfCities(); i++) {
			double isMutated = Math.random();
			if (isMutated < mutateRate) {
				int postion2 = (int) Math.random() * CityList.numberOfCities();
				City city1 = child.getCity(i);
				City city2 = child.getCity(postion2);

				// swap cities
				child.setCity(i, city2);
				child.setCity(postion2, city1);
			}
		}
		return child;
	}

}
