package Utility;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import KP.Item;
import KP.ItemManager;
import KP.KPData;
import KP.KPGreedy;
import KP.KPopulation;
import KP.Package;
import TSP.City;
import TSP.CityList;
import TSP.TPopulation;
import TSP.Travel;

public class GAJunitTest {

	@Test
	public void TSPgetFitestTest() {
		City c0 = new City(0, "city1");
		City c1 = new City(1, "city2");
		City c2 = new City(2, "city3");
		City c3 = new City(3, "city4");
		CityList.addCity(c0);
		CityList.addCity(c1);
		CityList.addCity(c2);
		CityList.addCity(c3);

		double[][] distance = new double[4][4];
		distance[0][0] = 0;
		distance[0][1] = 60;
		distance[0][2] = 80;
		distance[0][3] = 100;
		distance[1][0] = 60;
		distance[1][1] = 0;
		distance[1][2] = 100;
		distance[1][3] = 80;
		distance[2][0] = 80;
		distance[2][1] = 100;
		distance[2][2] = 0;
		distance[2][3] = 60;
		distance[3][0] = 100;
		distance[3][1] = 80;
		distance[3][2] = 60;
		distance[3][3] = 0;
		CityList.distance = distance;

		ArrayList<City> tr1 = new ArrayList();
		tr1.add(c0);
		tr1.add(c1);
		tr1.add(c3);
		tr1.add(c2);
		Travel t1 = new Travel(tr1);

		ArrayList<City> tr2 = new ArrayList();
		tr2.add(c0);
		tr2.add(c3);
		tr2.add(c1);
		tr2.add(c2);
		Travel t2 = new Travel(tr2);

		ArrayList<City> tr3 = new ArrayList();
		tr3.add(c0);
		tr3.add(c3);
		tr3.add(c2);
		tr3.add(c1);
		Travel t3 = new Travel(tr2);

		ArrayList<Travel> tl = new ArrayList();
		tl.add(t1);
		tl.add(t2);
		tl.add(t3);

		TPopulation pop = new TPopulation(tl);
		Travel target = pop.getFittest();
		assertEquals(t1, target);
	}

	@Test
	public void TSPmutationTest() {
		City c0 = new City(0, "city1");
		City c1 = new City(1, "city2");
		City c2 = new City(2, "city3");
		City c3 = new City(3, "city4");
		CityList.addCity(c0);
		CityList.addCity(c1);
		CityList.addCity(c2);
		CityList.addCity(c3);

		ArrayList<City> tr1 = new ArrayList();
		tr1.add(c0);
		tr1.add(c1);
		tr1.add(c3);
		tr1.add(c2);
		Travel t1 = new Travel(tr1);

		boolean result = true;
		TSPHandler tsp = new TSPHandler();
		for (int i = 0; i < 1000; i++) {
			t1 = tsp.mutate(0.02, t1);
		}

		if (!t1.containCity(c0)) {
			result = false;
		}
		if (!t1.containCity(c1)) {
			result = false;
		}
		if (!t1.containCity(c2)) {
			result = false;
		}
		if (!t1.containCity(c3)) {
			result = false;
		}
		assertTrue(result);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void KPsortItemTest() {
		Item[] list = new Item[5];
		list[0] = new Item("item1", 100, 50);
		list[1] = new Item("item2", 80, 50);
		list[2] = new Item("item3", 120, 50);
		list[3] = new Item("item4", 70, 50);
		list[4] = new Item("item5", 130, 50);

		KPData.itemlist = list;
		String[] benefit = { "item5", "item3", "item1", "item2", "item4" };
		KPGreedy gd = new KPGreedy();
		Item[] result = gd.getItemlist();

		String[] result1 = new String[result.length];
		for (int i = 0; i < result.length; i++) {
			result1[i] = result[i].getName();
		}
		assertEquals(benefit, result1);
	}

	@Test
	public void KPgetFittestTest() {
		Item t1 = new Item("item1", 100, 50);
		Item t2 = new Item("item2", 80, 50);
		Item t3 = new Item("item3", 120, 50);
		Item t4 = new Item("item4", 70, 50);
		Item t5 = new Item("item5", 130, 50);
		ItemManager.addItem(t1);
		ItemManager.addItem(t2);
		ItemManager.addItem(t3);
		ItemManager.addItem(t4);
		ItemManager.addItem(t5);

		ArrayList r1 = new ArrayList();
		r1.add(1);
		r1.add(1);
		r1.add(1);
		r1.add(1);
		r1.add(1);
		Package pkg1 = new Package(r1);

		ArrayList r2 = new ArrayList();
		r2.add(1);
		r2.add(1);
		r2.add(1);
		r2.add(1);
		r2.add(0);
		Package pkg2 = new Package(r2);

		ArrayList r3 = new ArrayList();
		r3.add(1);
		r3.add(1);
		r3.add(1);
		r3.add(0);
		r3.add(0);
		Package pkg3 = new Package(r3);

		ArrayList r4 = new ArrayList();
		r4.add(1);
		r4.add(1);
		r4.add(0);
		r4.add(0);
		r4.add(0);
		Package pkg4 = new Package(r4);

		ArrayList r5 = new ArrayList();
		r5.add(1);
		r5.add(0);
		r5.add(0);
		r5.add(0);
		r5.add(0);
		Package pkg5 = new Package(r5);

		ArrayList<Package> pl = new ArrayList();
		pl.add(pkg1);
		pl.add(pkg2);
		pl.add(pkg3);
		pl.add(pkg4);
		pl.add(pkg5);

		KPopulation pop = new KPopulation(pl);
		Package result = pkg1;
		Package target = pop.getFittest();
		assertEquals(result, target);
	}

	@Test
	public void KPmakeValidIndividualTest() {
		Item t1 = new Item("item1", 100, 51);
		Item t2 = new Item("item2", 80, 52);
		Item t3 = new Item("item3", 120, 120);
		Item t4 = new Item("item4", 70, 11);
		Item t5 = new Item("item5", 130, 90);
		ItemManager.addItem(t1);
		ItemManager.addItem(t2);
		ItemManager.addItem(t3);
		ItemManager.addItem(t4);
		ItemManager.addItem(t5);

		ArrayList r1 = new ArrayList();
		r1.add(1);
		r1.add(1);
		r1.add(1);
		r1.add(1);
		r1.add(1);
		Package pkg1 = new Package(r1);

		pkg1.makeValidIndividual(170);
		double volume = pkg1.getTotalVolume();
		assertTrue(volume <= 170);

	}

	@Test
	public void quickSortTest() {
		Item t1 = new Item("item1", 100, 50);
		Item t2 = new Item("item2", 80, 50);
		Item t3 = new Item("item3", 120, 50);
		Item t4 = new Item("item4", 70, 50);
		Item t5 = new Item("item5", 130, 50);
		ItemManager.addItem(t1);
		ItemManager.addItem(t2);
		ItemManager.addItem(t3);
		ItemManager.addItem(t4);
		ItemManager.addItem(t5);

		ArrayList r1 = new ArrayList();
		r1.add(1);
		r1.add(1);
		r1.add(1);
		r1.add(1);
		r1.add(1);
		Package pkg1 = new Package(r1);

		ArrayList r2 = new ArrayList();
		r2.add(1);
		r2.add(1);
		r2.add(1);
		r2.add(1);
		r2.add(0);
		Package pkg2 = new Package(r2);

		ArrayList r3 = new ArrayList();
		r3.add(1);
		r3.add(1);
		r3.add(1);
		r3.add(0);
		r3.add(0);
		Package pkg3 = new Package(r3);

		ArrayList r4 = new ArrayList();
		r4.add(1);
		r4.add(1);
		r4.add(0);
		r4.add(0);
		r4.add(0);
		Package pkg4 = new Package(r4);

		ArrayList r5 = new ArrayList();
		r5.add(1);
		r5.add(0);
		r5.add(0);
		r5.add(0);
		r5.add(0);
		Package pkg5 = new Package(r5);

		Package[] individual = new Package[5];
		individual[0] = pkg1;
		individual[1] = pkg2;
		individual[2] = pkg3;
		individual[3] = pkg4;
		individual[4] = pkg5;
		QuickSort.quickSort(individual);
		boolean result = true;

		double fitness = pkg1.getFitness();
		for (int i = 0; i < individual.length; i++) {
			if (fitness < individual[i].getFitness()) {
				result = false;
			}
		}
		assertTrue(result);
	}

}
