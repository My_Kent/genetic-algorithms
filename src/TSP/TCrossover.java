package TSP;

public class TCrossover {
	public static Travel crossover(int crossMtd, double crossRate, Travel parent1, Travel parent2){
		Travel child = null;
		switch(crossMtd){
		case 0:
			// single-point crossover
			child = sinCross(crossRate, parent1, parent2);
			break;
		case 1:
			// two-point crossover
			child = twoCross(crossRate, parent1, parent2);
			break;
		
		}
		return child;
	}

	

	private static Travel twoCross(double crossRate, Travel parent1,
			Travel parent2) {
		Travel child = new Travel();
		int size = CityList.numberOfCities();
		int startPos = (int)(Math.random() * size);
		int endPos = (int)(Math.random() * size);
		 for (int i = 0; i < CityList.numberOfCities(); i++) {
	            // If our start position is less than the end position
	            if (startPos < endPos && i > startPos && i < endPos) {
	                child.setCity(i, parent1.getCity(i));
	            } // If our start position is larger
	            else if (startPos > endPos) {
	                if (!(i < startPos && i > endPos)) {
	                    child.setCity(i, parent1.getCity(i));
	                }
	            }
	        }

	        // Loop through parent2's city tour
	        for (int i = 0; i < size; i++) {
	            // If child doesn't have the city add it
	            if (!child.containCity(parent2.getCity(i))) {
	                // Loop to find a spare position in the child's tour
	                for (int ii = 0; ii <size; ii++) {
	                    // Spare position found, add city
	                    if (child.getCity(ii) == null) {
	                        child.setCity(ii, parent2.getCity(i));
	                        break;
	                    }
	                }
	            }
	        }
		
		return child;
	}

	private static Travel sinCross(double crossRate, Travel parent1,
			Travel parent2) {
		Travel child = new Travel();
		int position = (int) (CityList.numberOfCities() * crossRate);
		for(int i = 0; i < CityList.numberOfCities(); i++){
			
			if(i < position){
				// copy the head genes from parent1
				child.setCity(i, parent1.getCity(i));
			} else{
				// add the genes that not contains in the same order as parent2
				for(int j = 0; j < CityList.numberOfCities(); j++){
					if(!child.containCity(parent2.getCity(j))){
						child.setCity(i, parent2.getCity(j));
						break;
					}
				}
			}
		}
		
		return child;
	}
}
