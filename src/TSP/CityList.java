package TSP;

import java.util.ArrayList;

public class CityList {
	private static ArrayList<City> destination = new ArrayList<City>();
	public static double[][] distance;

	public static void addCity(City city) {
		destination.add(city);
	}

	public static City getCity(int index) {
		City city = (City) destination.get(index);
		return city;
	}

	public static int numberOfCities() {
		return destination.size();
	}

	public static void clear() {
		destination.clear();
	}
}
