package TSP;

public class TSPGreedy {
	private  City[] cityList;
	private double[] results;
	private static double[][] distance;
	
	
	public TSPGreedy(){
		this.cityList = TSPData.cityList;
		this.distance = CityList.distance;
		results = new double[cityList.length];
		
		iteration();
	}
	
	private void iteration(){
		int length = cityList.length;

		for(int i = 0; i < length; i++){
			// a greedy search
			greedy(i);
		}
		findMin();
		
	}
	
	
	private void findMin(){
		double min = results[0];
		for(int i = 0; i < results.length; i++){
			
			if(min > results[i]){
				min = results[i];
			}
		}
		TSPData.greedyResult = min;
		
	}
	
	
	
	private void greedy(int i){
		int length = cityList.length;
		boolean[] isVisited = new boolean[length];
		isVisited[i] = true;
		int current = i;

		
		double sum = 0;
		for(int j = 0; j < length - 1; j++){
			// find the nearest city that have not been visited;
			int minIndex = -1;
			double dis = 0;
			 
			
			
			for(int z = 0; z < length; z ++){
				if(isVisited[z] == false){
					if(dis <= 0){
						minIndex = z;
						dis = distance[current][z];
					} else{
						if(dis > distance[current][z]){
							minIndex = z;
							dis = distance[current][z];
						}
					}
					
				}
			}
			

			sum += dis;
			current = minIndex;
			isVisited[minIndex] = true;
		}
		
		sum += distance[current][i];
		results[i] = sum;
	}
	

}
