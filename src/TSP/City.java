package TSP;

public class City {
	private String cityName = "";
	private int index = 0;

	public City(int index, String name) {
		this.index = index;
		this.cityName = name;

	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getCityName() {
		return cityName;
	}

	/**
	 * Return the distance with two cities
	 * 
	 * @param city1
	 * @return
	 */
	public double distanceTo(City city1) {
		int i1 = this.index;
		int i2 = city1.getIndex();
		double result = CityList.distance[i1][i2];
		
		return result;
	}

}
