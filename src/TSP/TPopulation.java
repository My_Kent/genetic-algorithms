package TSP;

import java.util.ArrayList;


public class TPopulation {
	private ArrayList<Travel> population = new ArrayList<Travel> ();
	private int popSize = 0;
	
	public TPopulation(int size, boolean isInitial){
		this.popSize = size;
		if(isInitial){
			for(int i = 0; i < size; i++){
				Travel travel = new Travel();
				travel.generateIndividual();
				population.add(travel);
			}
		}
	}
	
	public TPopulation(ArrayList<Travel> pop){
		this.population = pop;
	}
	

	public ArrayList<Travel> getPopulation() {
		return population;
	}

	public void setPopulation(ArrayList<Travel> population) {
		this.population = population;
	}

	public int getSize() {
		return popSize;
	}


	
	public Travel getFittest(){
			
		Travel fittest = population.get(0);
		for(int i = 0; i < popSize; i++){
			if(fittest.getFitness() < population.get(i).getFitness()){
				fittest = population.get(i);
			}
		}
		return fittest;
	}
	
	public void addTravel(int index, Travel travel){
		population.add(index, travel);
	}
	
	public Travel getTravel(int index){
		return (Travel)population.get(index);
	}
	
	
	
	
	
}
