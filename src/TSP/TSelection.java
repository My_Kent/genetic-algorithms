package TSP;

import java.util.ArrayList;

import Utility.QuickSort;

public class TSelection {
	public static Travel select(TPopulation pop, int selectMtd) {
		Travel ob = null;
		switch (selectMtd) {
		case 0:
			// Routtle Wheel Section
			ob = rwSelection(pop);
			break;
		case 1:
			ob = rankSelection(pop);
			// Rank Selection
			break;
		case 2:
			ob = tSelection(pop);
			// Tournament Selection
			break;
		case 3:
			ob = groupSelection(pop);
			// Group Selection
			break;
		}
		return ob;
	}

	private static Travel groupSelection(TPopulation pop) {
		int size = pop.getSize();
		Travel[] temp = new Travel[size];
		for (int i = 0; i < size; i++) {
			temp[i] = pop.getTravel(i);
		}
		QuickSort.quickSort(temp);
		
		
		int i1 = size/4;
		int i2 = size/2;
		int i3 = size*3/4;
		
		Travel select = null;
		
		double prob = Math.random();
		if(prob < 0.05){
			int index = (int)(i1 * Math.random());
			select = temp[index];
		}else if(prob >= 0.05 && prob < 0.2){
			int index = (int)((i2-i1) * Math.random());
			select = temp[index+i1];
		}else if(prob >=0.2 && prob <= 0.5){
			int index = (int)((i3-i2) * Math.random());
			select = temp[index+i2];
		}else{
			int index = (int)((size-i3) * Math.random());
			select = temp[index+i3];
			
		}
		return select;
	}

	private static Travel tSelection(TPopulation pop) {
		int size1 = pop.getSize();
		ArrayList<Travel> temp = new ArrayList<Travel>();
		// randomly choose some individuals from the whole population
		double bound = 0.2;
		for(int i = 0; i < size1; i++){
			double prob = Math.random();
			if(prob < bound){
				temp.add(pop.getTravel(i));
			}
		}
		int size2 = temp.size();
		Travel[] group = new Travel[size2];
		for(int i = 0; i < size2; i++){
			group[i] = temp.get(i);
		}
		QuickSort.quickSort(group);
		Travel select = group[size2-1];		
		return select;
	}

	// Linear Rank Selection
	private static Travel rankSelection(TPopulation pop) {
		
		
		// sort the array of the population according to the fitness of the
		// individual
		int size = pop.getSize();
		Travel[] temp = new Travel[size];
		for (int i = 0; i < size; i++) {
			temp[i] = pop.getTravel(i);
		}
		QuickSort.quickSort(temp);

		double[] probability = new double[size];
		double sum = 0;
		for (int i = 0; i < size; i++) {
			probability[i] = (double) ((i + 1) / (1.0 * size * (size - 1)));
			sum += probability[i];

		}

		Travel select = null;
		while (true) {
			double prob = Math.random();
			if (prob < sum) {
				double sum1 = probability[0];
				for (int i = 0; i < size; i++) {
					if (prob < sum1) {
						select = temp[i];
						break;
					}
					sum1 += probability[i + 1];
				}
				return select;
			}
		}

	}

	private static Travel rwSelection(TPopulation pop) {
		double totalFitness = 0;
		for (int i = 0; i < pop.getSize(); i++) {

			totalFitness += pop.getTravel(i).getFitness();
		}
		double[] proportion = new double[pop.getSize()];
		for (int i = 0; i < pop.getSize(); i++) {
			if (totalFitness == 0) {
				break;
			} else {
				proportion[i] = (double) (pop.getTravel(i).getFitness() / totalFitness);
			}
		}

		double temp = Math.random();
		int index = 0;
		double sum = 0;
		for (int j = 0; j < proportion.length; j++) {
			sum += proportion[j];
			if (temp < sum) {
				index = j;
				break;
			}
		}
		return pop.getTravel(index);
	}
}
