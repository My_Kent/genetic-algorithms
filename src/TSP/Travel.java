package TSP;
import java.util.ArrayList;
import java.util.Collections;

import Utility.Individual;


public class Travel extends Individual{
	private ArrayList<City> travel = new ArrayList<City>();
	
	public Travel(){
		int size = CityList.numberOfCities();
		for(int i = 0; i < size; i++){
			travel.add(null);
		}
	}
	
	public Travel(ArrayList<City> tr){
		this.travel = tr;
	}
	
	public void generateIndividual(){
		int size = CityList.numberOfCities();
		for(int i = 0; i < size; i++){
			travel.set(i, CityList.getCity(i));
		}
		Collections.shuffle(travel);	
	}
	
	public City getCity(int index){
		return (City)travel.get(index);
	}
	
	public void setCity(int index, City city){
		travel.set(index, city);
	}

	public double getFitness(){
		int size = CityList.numberOfCities();
		double distance = getDistance();
		double fitness = 1/distance;
		return fitness;
	}
	
	public double getDistance(){
		int size = CityList.numberOfCities();
		double distance = 0;
		for(int i = 0; i < size -1; i++){
			if(travel.get(i)!= null && travel.get(i+1) != null){
				distance += travel.get(i).distanceTo(travel.get(i+1));
			}
		}
		distance += travel.get(size-1).distanceTo(travel.get(0));
		return distance;
	}
	
	public boolean containCity(City city){
		return travel.contains(city);
	}
	
	
	
	@Override
	public String toString(){
		String result = "The order of cities that going to travel is: \n";
		int size = CityList.numberOfCities();
		for(int i = 0; i < size; i++){
			result += travel.get(i).getCityName() + ", ";
		}
		result += "\nThe total distance of this travel is: " + getDistance() + "\n";
		return result;
	}
	
	
	
}
