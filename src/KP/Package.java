package KP;
import java.util.ArrayList;

import Utility.Individual;


public class Package extends Individual{
	private ArrayList individual = new ArrayList();
	
	public Package(){
		int size = ItemManager.getSize();
		for(int i = 0; i < size; i++){
			individual.add(0);
		}
	}
	
	
	
	public Package(ArrayList individual){
		this.individual = individual;
	}
	
	public void generateIndividual(){
		int size = individual.size();
		for(int i = 0; i < size; i++){
			double temp = Math.random();
			if(temp < 0.5){
				individual.set(i, 0);
			} else{
				individual.set(i, 1);
			}
		}
		
	}
	
	public double getTotalVolume(){
		int totalVolume = 0;
		for(int i = 0; i < individual.size(); i++){
			int value = (int) individual.get(i);
			if(value == 1){
				totalVolume += ItemManager.getVolumeN(i);
			}
		}
		return totalVolume;
	}
	
	public double getFitness(){
		int totalBenefit = 0;
		for(int i = 0; i < individual.size(); i++){
			int value = (int) individual.get(i);
			if(value == 1){
				totalBenefit += ItemManager.getBenefitN(i);
			}
		}
		return totalBenefit;
	}
	
	// TODO, check whether current individual is valid,
	// if it is invalid, make it valid 
	public void makeValidIndividual(double totalVolume){
		double currentVolume = getTotalVolume();
		if(currentVolume > totalVolume){
			int value = 0;
			int randomIndex = 0;
			do{
				randomIndex = (int) ((individual.size()) * Math.random());
				value = (int) individual.get(randomIndex);
			} while(value != 1);
			// take out the item to make the total volume smaller than the maximum volume of the bag
			individual.set(randomIndex, 0);
			
			// recursive
			makeValidIndividual(totalVolume);	
		}
	}
	
	public int getValueN(int n){
		return (int)individual.get(n);
	}
	
	public void setValueN(int n, int value){
		individual.set(n, value);
	}
	
	public String toString(){
		String result = "The items are selected are: \n ";
		for(int i = 0; i < individual.size();i++){
			int temp = (int)individual.get(i);
			if(temp ==1){
				result += ItemManager.getItem(i).getName();
				result += "; ";
			}
		}
		result += "\n with total benefit " + getFitness() +" \n";
		return result;
	}
}
