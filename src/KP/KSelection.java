package KP;

import java.util.ArrayList;

import Utility.QuickSort;

public class KSelection {
	
	public static Package select(KPopulation pop, int selectMtd){
		Package pa = null;
		switch(selectMtd){
		case 0:
			pa = rwSelection(pop);
			break;
		case 1:
			pa = rankSelection(pop);
			break;
		case 2:
			pa = tSelection(pop);
			break;
		case 3:
			pa = groupSelection(pop);
			break;
		}
		
		return pa;
	}

	private static Package groupSelection(KPopulation pop) {
		int size = pop.getSize();
		Package[] temp = new Package[size];
		for(int i = 0; i < size; i++){
			temp[i] = pop.getIndividual(i);
		}
		
		QuickSort.quickSort(temp);
		
		int i1 = size/4;
		int i2 = size/2;
		int i3 = size *3/4;
		Package select = null;
		
		double prob = Math.random();
		if(prob < 0.05){
			int index = (int)(i1 * Math.random());
			select = temp[index];
		}else if(prob >= 0.05 && prob < 0.2){
			int index = (int)((i2-i1) * Math.random());
			select = temp[index+i1];
		}else if(prob >=0.2 && prob <= 0.5){
			int index = (int)((i3-i2) * Math.random());
			select = temp[index+i2];
		}else{
			int index = (int)((size-i3) * Math.random());
			select = temp[index+i3];
			
		}
		return select;
	}

	private static Package tSelection(KPopulation pop) {
		int size1 = pop.getSize();
		ArrayList<Package> temp = new ArrayList<Package>();
		double bound = 0.2;
		for(int i = 0; i < size1; i++){
			double prob = Math.random();
			if(prob < bound){
				temp.add(pop.getIndividual(i));
			}
		}
		
		int size2 = temp.size();
		Package[] group = new Package[size2];
		for(int i = 0; i < size2; i++){
			group[i] = temp.get(i);
		}
		QuickSort.quickSort(group);
		Package select = group[size2-1];
		return select;
	}

	// Linear Rank Selection
	private static Package rankSelection(KPopulation pop) {
		int size = pop.getSize();
		Package[] temp = new Package[size];
		for(int i = 0; i < size; i++){
			temp[i] = pop.getIndividual(i);
		}
		QuickSort.quickSort(temp);
		
		double[] probability = new double[size];
		double sum = 0;
		for (int i = 0; i < size; i++) {
			probability[i] = (double) ((i + 1) / (1.0 * size * (size - 1)));
			sum += probability[i];
		}
		
		Package select = null;
		
		while(true){
			double prob = Math.random();
			if (prob < sum) {
				double sum1 = probability[0];
				for (int i = 0; i < size; i++) {
					if (prob < sum1) {
						select = temp[i];
						break;
					}
					sum1 += probability[i + 1];
				}
				return select;
			}
		}		
	}

	private static Package rwSelection(KPopulation pop) {
		double totalFitness = 0;
		int size = pop.getSize();
		for(int i = 0; i < size; i++){
			totalFitness += pop.getIndividual(i).getFitness();
		}
		
		double[] proportion = new double[pop.getSize()];
		for(int i = 0; i < pop.getSize(); i++){
			if(totalFitness == 0){
				break;
			}else{
				proportion[i] = (double) (pop.getIndividual(i).getFitness()/totalFitness);
			}
		}
		
		double temp = Math.random();
		int index = 0;
		double sum = 0;
		for(int j = 0; j < proportion.length; j++){
			sum += proportion[j];
			if(temp < sum){
				index = j;
				break;
			}
			
		}
		return pop.getIndividual(index);
	}
}
