package KP;

public class Item {
	private double benefit = 0;
	private double volume = 0;
	private String name = "";

	public Item() {
	}

	public Item(String name, double benefit, double volume) {
		this.name = name;
		this.benefit = benefit;
		this.volume = volume;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBenefit() {
		return benefit;
	}

	public void setBenefit(double benefit) {
		this.benefit = benefit;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

}
