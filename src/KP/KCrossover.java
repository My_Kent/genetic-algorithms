package KP;

public class KCrossover {
	
	public static Package crossover(int crossMtd, double crossRate, Package parent1, Package parent2){
		Package child = null;
		switch(crossMtd){
		case 0:
			child = sinCross(crossRate, parent1, parent2);
			break;
		case 1:
			child = twoCross(crossRate, parent1, parent2);
			break;
		
		}
		
		return child;
	}



	private static Package twoCross(double crossRate, Package parent1,
			Package parent2) {
		Package child = new Package();
		int position1 = (int)(ItemManager.getSize() * Math.random());
		int position2 = (int)(ItemManager.getSize() * Math.random());
		
		if(position1 < position2){
			for(int i = 0; i < position1; i++){
				child.setValueN(i, parent2.getValueN(i));
			}
			for(int j = position1; j < position2; j++){
				child.setValueN(j, parent1.getValueN(j));
			}
			for(int k= position2; k < ItemManager.getSize(); k++){
				child.setValueN(k, parent2.getValueN(k));
			}
		}else{
			for(int i = 0; i < position2; i++){
				child.setValueN(i, parent2.getValueN(i));
			}
			for(int j = position2; j < position1; j++){
				child.setValueN(j, parent1.getValueN(j));
			}
			for(int k= position1; k < ItemManager.getSize(); k++){
				child.setValueN(k, parent2.getValueN(k));
			}
		}
		
		
		return child;
	}

	private static Package sinCross(double crossRate, Package parent1,
			Package parent2) {
		Package child = new Package();
		int position = (int)(ItemManager.getSize() * crossRate);
		for(int i = 0; i < ItemManager.getSize(); i++){
			if(i < position){
				child.setValueN(i, parent1.getValueN(i));
			}else{
				child.setValueN(i, parent2.getValueN(i));
			}
		}
		return child;
	}

	
}
