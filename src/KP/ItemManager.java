package KP;

import java.util.ArrayList;

public class ItemManager {
	private static ArrayList<Item> itemList = new ArrayList<Item>();

	public static int getSize() {
		return itemList.size();
	}

	public static double getVolumeN(int n) {
		Item temp = (Item) itemList.get(n);
		double volume = temp.getVolume();
		return volume;
	}

	public static double getBenefitN(int n) {
		Item temp = (Item) itemList.get(n);
		double benefit = temp.getBenefit();
		return benefit;
	}

	public static void addItem(Item item) {
		itemList.add(item);
	}

	public static Item getItem(int n) {
		return (Item) itemList.get(n);
	}
	
	public static void clear(){
		itemList.clear();
	}

}
