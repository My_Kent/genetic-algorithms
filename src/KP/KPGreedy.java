package KP;

public class KPGreedy {
	private Item[] itemlist;
	
	public Item[] getItemlist() {
		
		return itemlist;
	}


	public KPGreedy(){
		
		itemlist = sortItem();
		
		// add the item starting with index 0 until the package is full
		double capacityLeft = KPData.totalVolume;
		double benefit = 0;
		for(int i = 0; i < itemlist.length; i ++){
			if(itemlist[i].getVolume() < capacityLeft){
				benefit += itemlist[i].getBenefit();
				capacityLeft = capacityLeft - itemlist[i].getVolume();
			}
		}
		
		KPData.greedyResult = benefit;
	}

	
	public Item[] sortItem() {
		// selection sort
		
		Item[] result = KPData.itemlist;
		int length = result.length;
		int maxIndex;
		Item temp;
		
		
		for(int i = 0; i < result.length-1; i++){
			maxIndex = i;
			for(int j = i + 1; j < result.length; j++){
				double r1 = result[j].getBenefit()/result[j].getVolume();
				double r2 = result[maxIndex].getBenefit()/result[maxIndex].getVolume();
			
				if(r1 > r2){
					maxIndex = j;
				}
			}
			
			if(maxIndex != i){
				temp = result[i];
				result[i] = result[maxIndex];
				result[maxIndex] = temp;
			}
		}

		return result;		
	}
}
