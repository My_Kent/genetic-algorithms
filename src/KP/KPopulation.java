package KP;

import java.util.ArrayList;


public class KPopulation {
	private ArrayList<Package> pop = new ArrayList<Package>();
	private int size = 0;
	private double maxVolume = 0;
	
	public KPopulation(ArrayList<Package> pop){
		this.pop = pop;
	}
	
	
	
	public double getMaxVolume() {
		return maxVolume;
	}

	public void setMaxVolume(double maxVolume) {
		this.maxVolume = maxVolume;
	}

	public ArrayList<Package> getPop() {
		return pop;
	}

	public void setPop(ArrayList<Package> pop) {
		this.pop = pop;
	}

	public KPopulation(int size, double maxVolume, boolean isInitial){
		this.maxVolume = maxVolume;
		this.size = size;
		if(isInitial){
			for(int i = 0; i < size; i++){
				Package in = new Package();
				in.generateIndividual();
				in.makeValidIndividual(maxVolume);
				pop.add(in);
			}
		}
	}
	
	public Package getFittest(){
		double fitness = ((Package)pop.get(0)).getFitness();
		int fittest = 0;
		for(int i = 1; i < pop.size(); i++){
			double tempFitness = ((Package)pop.get(i)).getFitness();
			if(tempFitness > fitness){
				fitness = tempFitness;
				fittest = i;
			}
		}
		Package fittestIn = (Package) pop.get(fittest);
		return fittestIn;
	}
	
	public void setInvididual(int n, Package in){
		pop.set(n, in);
	}
	
	public void addIndividual(Package n){
		pop.add(n);
	}
	
	public Package getIndividual(int n){
		Package result = (Package) pop.get(n);
		return result;
	}
	
	public int getSize(){
		return size;
	}
}
